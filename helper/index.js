exports.nullCecker = function (param) {
    const payload = param;
    for (let i = 0; i < Object.keys(payload).length; i++) {
        if (payload[Object.keys(payload)[i]].length == 0) {
            return { status: false, data: Object.keys(payload)[i] }
        } else if ((i + 1) == Object.keys(payload).length) {
            if (payload[Object.keys(payload)[i]].length == 0) {
                return { status: false, data: Object.keys(payload)[i] }
            } else {
                return { status: true };
            }
        }
    }
}