const express = require('express');
const router = express.Router();
const model = require('../models')
const help = require('../helper');

router.get('/', async function (req, res, next) {
  try {
    const users = await model.users.findAll({});
    res.json({
      'status': 'OK',
      'messages': '',
      'data': users
    })
  } catch (err) {
    res.json({
      'status': 'ERROR',
      'messages': err,
      'data': {}
    })
  }
});

router.post('/', async function (req, res, next) {
  var CECK = help.nullCecker(req.body);
  const payload = {
    name,
    email,
    gender,
    phone_number
  } = req.body;
  if (CECK.status) {
    try {
      const users = await model.users.create(payload);
      if (users) {
        res.status(201).json({
          'status': 'OK',
          'messages': 'User Has Been Success Added',
          'data': users,
        })
      }
    } catch (err) {
      res.status(400).json({
        'status': 'ERROR',
        'messages': err.message,
        'data': {},
      })
    }
  } else {
    res.status(400).json({
      'status': 'ERROR',
      'messages': 'ERROR',
      'data': 'Payload with key ' + CECK.data + ' cannot empty',
    })
  }
});

router.patch('/:id', async function (req, res, next) {
  try {
    const usersId = req.params.id;
    const {
      name,
      email,
      gender,
      phoneNumber
    } = req.body;
    const users = await model.users.update({
      name,
      email,
      gender,
      phone_number: phoneNumber
    }, {
      where: {
        id: usersId
      }
    });
    if (users) {
      res.json({
        'status': 'OK',
        'messages': 'User berhasil diupdate',
        'data': users,
      })
    }
  } catch (err) {
    res.status(400).json({
      'status': 'ERROR',
      'messages': err.message,
      'data': {},
    })
  }
});

router.delete('/:id', async function (req, res, next) {
  try {
    const usersId = req.params.id;
    const users = await model.Todo.destroy({
      where: {
        id: usersId
      }
    })
    if (users) {
      res.json({
        'status': 'OK',
        'messages': 'User berhasil dihapus',
        'data': users,
      })
    }
  } catch (err) {
    res.status(400).json({
      'status': 'ERROR',
      'messages': err.message,
      'data': {},
    })
  }
});

module.exports = router;
